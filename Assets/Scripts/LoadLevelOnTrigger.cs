﻿using UnityEngine;
using System.Collections;

public class LoadLevelOnTrigger : MonoBehaviour {

	public string levelToLoad;

	void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "Player")
			Application.LoadLevel(levelToLoad);
	}

}

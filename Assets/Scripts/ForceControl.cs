﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


public class ForceControl : Singleton<ForceControl>{

	public Transform playerTransform;
	public ShockWave shockwave;

	public float forcePush;
	public float forcePull;

	public float longPressTime;

	public LayerMask hitMask;

	private Camera mainCamera;
	private Rigidbody2D body;

	private bool isLongPress = false;
	private bool isPush = true;

	private Vector3 debugPosOne, debugPosTwo;

	// Use this for initialization
	void Start () {
		mainCamera = GetComponent<Camera>();

		body = playerTransform.GetComponent<Rigidbody2D>();
	}


	public void OnClick(){
		Push();
		Pull();
	}

	public void OnLongPress(){
		
	}

	void Update(){
		PushWave();
	}

	private void PushWave(){
		if(Input.GetKeyDown(KeyCode.Space)){
			if(isPush)
				shockwave.Push();
			else
				shockwave.Pull();
		}
	}

	private void Push(){
		if(Input.GetMouseButtonUp(0)){
			isPush = true;
			Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);

			Vector2 pos = ray.GetPoint(10);

			Vector2 forceDir = (Vector2)playerTransform.position - pos;

			RaycastHit2D target = Physics2D.Raycast(playerTransform.position, -forceDir, 100, hitMask);
			if(target == null)
				return;

			if(target.transform.tag != "Projectile"){
				if(target.rigidbody == null || target.rigidbody.GetComponent<Interactable>().level > forcePush){
					body.AddForce(forceDir.normalized * forcePush, ForceMode2D.Impulse);
				}else{
					target.rigidbody.AddForceAtPosition(-forceDir.normalized * forcePush, target.point, ForceMode2D.Impulse);
					body.AddForce(forceDir.normalized * forcePush / 10, ForceMode2D.Impulse);
				}
			}else{
				target.transform.SendMessage("Deflected");
				//get new direction
				Vector2 reflectedDir = Vector3.Reflect(target.rigidbody.velocity.normalized, -forceDir.normalized);
				target.rigidbody.velocity = Vector2.zero;
				target.rigidbody.AddForce(-reflectedDir * forcePush * 4, ForceMode2D.Impulse);
			}
		}
	}

	private void Pull(){
		if(Input.GetMouseButtonUp(1)){
			isPush = false;
			Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
			
			Vector2 pos = ray.GetPoint(10);
			
			Vector2 forceDir = (Vector2)playerTransform.position - pos;
			
			RaycastHit2D target = Physics2D.Raycast(playerTransform.position, -forceDir, 100, hitMask);
			if(target == null)
				return;

			if(target.transform.tag != "Projectile"){
				if(target.rigidbody == null || target.rigidbody.GetComponent<Interactable>().level > forcePush){
					body.AddForce(-forceDir.normalized * forcePush, ForceMode2D.Impulse);
				}else{
					target.rigidbody.AddForceAtPosition(forceDir.normalized * forcePush, target.point, ForceMode2D.Impulse);
					body.AddForce(-forceDir.normalized * forcePush / 10, ForceMode2D.Impulse);
				}
			}else{
				target.transform.SendMessage("Deflected");
				//get new direction
				Vector2 reflectedDir = Vector3.Reflect(target.rigidbody.velocity.normalized, -forceDir.normalized);
				target.rigidbody.velocity = Vector2.zero;
				target.rigidbody.AddForce(reflectedDir * forcePush * 4, ForceMode2D.Impulse);
			}
		}
	}

}

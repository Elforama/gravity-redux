﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Interactable : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	public int level = 10;

	private bool isLongPress = false;

	public virtual void OnPointerDown(PointerEventData data){
		StartCoroutine("LongPressTimer");
	}
	
	public virtual void OnPointerUp(PointerEventData data){
		if(!isLongPress){
			StopCoroutine("LongPressTimer");
			ForceControl.instance.OnClick();
		}
	}

	IEnumerator LongPressTimer(){
		isLongPress = false;
		yield return new WaitForSeconds(ForceControl.instance.longPressTime);
		isLongPress = true;
		ForceControl.instance.OnLongPress();
	}
}

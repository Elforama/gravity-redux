﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour {

	public GameObject laserPrefab;
	public float laserSpeed;
	public float rateOfFire = 1;

	private Transform target;
	private Transform cachedTransform;
	private Rigidbody2D laserBody;
	private bool inRange = false;

	// Use this for initialization
	void Start () {
		target = GameObject.FindGameObjectWithTag("Player").transform;
		cachedTransform = transform;
	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "Player" && !inRange){
			StopCoroutine("Shooting");
			StartCoroutine("Shooting");
			inRange = true;
		}
	}

	void OnTriggerExit2D(Collider2D other){
		if(other.tag == "Player" && inRange){
			StopCoroutine("Shooting");
			inRange = false;
		}
	}

	IEnumerator Shooting(){
		while(true){
			Vector2 forceDir = target.position - cachedTransform.position;
			GameObject laser = Instantiate(laserPrefab, cachedTransform.position, Quaternion.identity) as GameObject;
			laserBody = laser.GetComponent<Rigidbody2D>();
			laserBody.AddForce(forceDir.normalized * laserSpeed, ForceMode2D.Impulse);
			yield return new WaitForSeconds(rateOfFire);
		}
	}
}

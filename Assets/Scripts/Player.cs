﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour{

	public int health = 10;

	public Text text;

	public void Shot(){
		health--;
		if(text != null)
			text.text = health.ToString();
		if(health < 1)
			Application.LoadLevel(0);
	}
}

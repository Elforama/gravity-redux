﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Damagable : MonoBehaviour {

	public float breakingPoint;

	private Rigidbody2D body;

	private Vector2 totalVelocity;
	private Vector2 lastVelocity;
	private float dot;

	// Use this for initialization
	void Start () {
		body = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate(){
		lastVelocity = body.velocity;
	}

	void OnCollisionEnter2D(Collision2D other){
		if(lastVelocity.magnitude >= breakingPoint){
			Debug.Log("DIE at " + lastVelocity.magnitude);
			Die ();
		}
		//else
			//Debug.Log("ALIVE at " + lastVelocity.magnitude);
		/*
		if(other.rigidbody != null)
			dot = Vector2.Dot(other.rigidbody.velocity, lastVelocity);
		else
			dot = lastVelocity.magnitude;
		Debug.Log(dot);
		*/
	}

	void Die(){
		Destroy(gameObject);
	}
}

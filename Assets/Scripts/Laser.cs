﻿using UnityEngine;
using System.Collections;

public class Laser : Interactable {

	private bool deflected = false;

	public void Deflected(){
		deflected = true;
		//GetComponent<Rigidbody2D>().velocity = Vector2.zero;
	}

	void OnTriggerEnter2D(Collider2D other){
		if(deflected){
			if(other.tag != "Range"){
				other.SendMessage("Shot", SendMessageOptions.DontRequireReceiver);
				Destroy(gameObject);
			}
		}else{
			if(other.tag != "Range" && other.tag != "Enemy"){
				other.SendMessage("Shot", SendMessageOptions.DontRequireReceiver);
				Destroy(gameObject);
			}
		}
	}

}

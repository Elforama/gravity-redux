﻿using UnityEngine;
using System.Collections;

public class ShockWave : MonoBehaviour {

	public float shockWaveTime = 1f;

	private PointEffector2D pointEffector;


	// Use this for initialization
	void Awake () {
		pointEffector = GetComponent<PointEffector2D>();
	}
	
	public void Push(){
		pointEffector.forceMagnitude = 10;
		StopCoroutine("ShockWaveTimer");
		StartCoroutine("ShockWaveTimer");
	}

	public void Pull(){
		pointEffector.forceMagnitude = -10;
		StopCoroutine("ShockWaveTimer");
		StartCoroutine("ShockWaveTimer");
	}

	IEnumerator ShockWaveTimer(){
		pointEffector.enabled = true;
		yield return new WaitForSeconds(shockWaveTime);
		pointEffector.enabled = false;
	}
}

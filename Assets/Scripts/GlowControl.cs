﻿using UnityEngine;
using System.Collections;

public class GlowControl : MonoBehaviour {

	public SpriteRenderer glow;
	public SpriteRenderer eye;

	public Color pushColor;
	public Color pullColor;

	// Use this for initialization
	void Start () {
		
	}	
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0)){
			//push
			glow.color = pushColor;
			eye.color = pushColor;
		}else if(Input.GetMouseButtonDown(1)){
			//pull
			glow.color = pullColor;
			eye.color = pullColor;
		}
	}
}

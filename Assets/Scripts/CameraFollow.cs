﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Camera))]
public class CameraFollow : MonoBehaviour {

	public static CameraFollow Instance;
	
	public Transform target;
	public float speed;
	
	public float cameraSizeSpeed = 3;
	public float maxCameraSize = 80;
	
	private Transform currentTarget;
	private Transform thisTransform;
	
	private Vector3 dragPos = new Vector3();
	
	private Vector3 velocity = new Vector2();
	
	private Camera camera;

	private Rigidbody2D targetBody;

	private float newCameraSize;
	
	void Awake(){
		Instance = this;
		camera = Camera.main;
		newCameraSize = camera.orthographicSize;
	}
	
	void Start(){
		thisTransform = transform;
		currentTarget = target;
		targetBody = currentTarget.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void LateUpdate(){
		dragPos = currentTarget.position;
		dragPos += new Vector3(targetBody.velocity.x / 2, targetBody.velocity.y / 2, 0);
		
		Vector3 goalPos = new Vector3(dragPos.x, dragPos.y, thisTransform.position.z);
		thisTransform.position = Vector3.SmoothDamp(thisTransform.position, goalPos, ref velocity, speed);
		
		camera.orthographicSize = Mathf.Lerp(camera.orthographicSize, newCameraSize, cameraSizeSpeed);
	}
}

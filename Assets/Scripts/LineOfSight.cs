﻿using UnityEngine;
using System.Collections;

public class LineOfSight : MonoBehaviour {

	public Material redLaser, blueLaser;
	public LineRenderer lineRenderer;
	public LayerMask layerMask;

	//public Color pullColor;
	//public Color pushColor;

	private Camera cam;
	private Transform cachedTransform;
	private Vector3 target;
	private Vector2 direction;
	void Awake(){
		cam = Camera.main;
		cachedTransform = transform;
	}

	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0)){//push RED
			lineRenderer.material = redLaser;
		}else if(Input.GetMouseButtonDown(1)){//pull BLUE
			lineRenderer.material = blueLaser;
		}


		Ray ray = cam.ScreenPointToRay(Input.mousePosition);
		target = ray.GetPoint(10);

		direction = target - cachedTransform.position;

		RaycastHit2D hit = Physics2D.Raycast(cachedTransform.position, direction, 100, layerMask);

		lineRenderer.SetPosition(1, cachedTransform.InverseTransformPoint(hit.point));
	}
}
